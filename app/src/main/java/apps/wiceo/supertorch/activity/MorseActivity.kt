package apps.wiceo.supertorch.activity

import android.os.Bundle
import apps.wiceo.supertorch.R
import apps.wiceo.supertorch.adapter.MorseCodeFragmentAdapter
import apps.wiceo.supertorch.data.MorseLetter
import apps.wiceo.supertorch.helper.MyCamera
import kotlinx.android.synthetic.main.activity_morse.*

/**
 * Created by Paul Chernenko
 * on 24.11.2017.
 */

class MorseActivity: BaseActivity() {

    private lateinit var camera: MyCamera

    override fun getLayout(): Int = R.layout.activity_morse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupViews()
    }

    override fun onStart() {
        setInterstitialUnitId(getString(R.string.ads_interstitial_morse_code))
        super.onStart()
        camera = MyCamera(this)
    }

    override fun onStop() {
        super.onStop()
        camera.stopPlayback()
        camera.release()
    }

    private fun setupViews(){
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        morsePager.offscreenPageLimit = 3
        morsePager.adapter = MorseCodeFragmentAdapter(supportFragmentManager)

        tabLayout.setupWithViewPager(morsePager)
    }

    fun playLetter(morseLetter: MorseLetter): Boolean{
        return playWord(listOf(morseLetter))
    }

    fun playWord(morseLetters: List<MorseLetter>): Boolean{
        if (!camera.playbackRunning()) {
            camera.playWord(morseLetters)
            return true
        } else {
            camera.stopPlayback()
            return false
        }
    }
}
