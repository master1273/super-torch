package apps.wiceo.supertorch.activity

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.annotation.LayoutRes
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.util.Log
import android.view.View
import android.widget.TextView
import apps.wiceo.supertorch.R
import apps.wiceo.supertorch.application.TorchApp
import apps.wiceo.supertorch.common.Constants
import apps.wiceo.supertorch.common.RequiredPermission
import apps.wiceo.supertorch.dialog.PermissionDialog
import apps.wiceo.supertorch.util.AdUtil
import apps.wiceo.supertorch.util.SharedPrefUtil
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.InterstitialAd
import org.solovyev.android.checkout.*

/**
 * Created by Paul Chernenko
 * on 22.11.2017.
 */
abstract class BaseActivity: AppCompatActivity(){

    private var interstitialUnitId: String? = null
    private var interstitialAd: InterstitialAd? = null
    private var purchased = true
    private var upgradeView: View? = null
    private var shouldConsume = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())

        if (upgradeView == null) {
            upgradeView = findViewById(R.id.layout_upgrade_app)
        }

        if (upgradeView != null) {
            upgradeView!!.findViewById<CardView>(R.id.upgrade_card).visibility = View.GONE
            upgradeView!!.setOnClickListener { _ -> purchaseAdsInApp() }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        mCheckout.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onStart() {
        super.onStart()
        initializeBilling()
    }

    override fun onDestroy() {
        mCheckout.stop()
        super.onDestroy()
    }

    @LayoutRes abstract fun getLayout(): Int

    open fun shouldShowInterstitialAd(): Boolean {
        return checkInterstitialAdPeriod(this.javaClass.simpleName)
    }

    fun checkInterstitialAdPeriod(key: String): Boolean {
        var currentPeriod = SharedPrefUtil.getSharedPreferences().getInt(key, 1)
        if (currentPeriod != Constants.ADD_DISPLAY_PERIOD) {
            SharedPrefUtil.putInt(key, ++currentPeriod)
            return false
        }
        return true
    }

    open fun interstitialAdShown() {
        resetInterstitialAdPeriod(this.javaClass.simpleName)
    }

    fun interstitialAdClosed() {}

    fun resetInterstitialAdPeriod(key: String) {
        val currentPeriod = SharedPrefUtil.getSharedPreferences().getInt(key, 1)
        if (currentPeriod == Constants.ADD_DISPLAY_PERIOD) {
            SharedPrefUtil.putInt(key, 0)
        }
    }

    fun setInterstitialUnitId(interstitialUnitId: String) {
        this.interstitialUnitId = interstitialUnitId
    }

    private fun loadInterstitialAd() {
        if (!purchased && interstitialUnitId != null && !AdUtil.isTestDevice(this)) {
            interstitialAd = AdUtil.getInterstitialAd(this, interstitialUnitId!!)
            interstitialAd!!.adListener = object : AdListener() {
                override fun onAdLoaded() {
                    super.onAdLoaded()
                    if (shouldShowInterstitialAd())
                        interstitialAd!!.show()
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    interstitialAdShown()
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    interstitialAd!!.loadAd(AdUtil.getAdRequest(this@BaseActivity))
                    interstitialAdClosed()
                }
            }

            interstitialAd!!.loadAd(AdUtil.getAdRequest(this))
        }
    }

    fun showInterstitialAd(): Boolean {
        if (!AdUtil.isTestDevice(this) && !purchased && interstitialAd != null && interstitialAd!!.isLoaded) {
            interstitialAd!!.show()
            return true
        }
        return false
    }

    private fun initializeAd() {
        val adView = findViewById<AdView>(R.id.ad_view)
        if (adView != null) {
            if (purchased || AdUtil.isTestDevice(this)) {
                adView.visibility = View.GONE
            } else {
                adView.adListener = object : AdListener() {
                    override fun onAdLoaded() {
                        super.onAdLoaded()
                        adView.visibility = View.VISIBLE
                    }
                }
                adView.loadAd(AdUtil.getAdRequest(this))
            }
        }
    }

    fun inventoryLoaded(product: Sku?, purchased: Boolean) {
        if (product != null)
            when (product.id.code) {
                Constants.IN_APP_ID -> {
                    this.purchased = purchased
                    initializeAd()
                    loadInterstitialAd()
                    onAdsInAppLoaded(product, purchased)
                }
            }
    }

    private fun onAdsInAppLoaded(product: Sku, purchased: Boolean) {
        if (upgradeView != null) {
            if (!purchased) {
                upgradeView!!.findViewById<CardView>(R.id.upgrade_card).visibility = View.VISIBLE
                upgradeView!!.findViewById<TextView>(R.id.upgrade_description).text = product.description
                upgradeView!!.findViewById<TextView>(R.id.upgrade_price).text = product.price
            } else {
                upgradeView!!.findViewById<CardView>(R.id.upgrade_card).visibility = View.GONE
            }
        }
    }

    private fun purchaseAdsInApp() {
        purchase(Constants.IN_APP_ID)
    }

    //Billing methods
    private val mCheckout = Checkout.forActivity(this@BaseActivity, TorchApp.instance!!.mBilling)

    private var mInventory: Inventory? = null
    private var product: Inventory.Product? = null

    private var mInventoryRequest: Inventory.Request? = null

    private val mInventoryCallback = InventoryCallback()

    private fun initializeBilling() {
        mCheckout.stop()
        mCheckout.start()

        mCheckout.createPurchaseFlow(PurchaseListener())

        mInventory = mCheckout.makeInventory()
        mInventoryRequest = Inventory.Request.create()
                .loadAllPurchases()
                .loadSkus(ProductTypes.IN_APP, Constants.IN_APP_ID)

        reloadInventory()
    }

    private inner class PurchaseListener : EmptyRequestListener<Purchase>() {
        override fun onSuccess(purchase: Purchase) {
            reloadInventory()
        }

        override fun onError(response: Int, e: Exception) {
            if (response == ResponseCodes.ITEM_ALREADY_OWNED) {
                Log.i(this.javaClass.toString(), "ERROR: ITEM_ALREADY_OWNED")
            } else {
                Log.e(this.javaClass.toString(), "ERROR: " + e.toString())
            }

            reloadInventory()
        }
    }

    private inner class InventoryCallback : Inventory.Callback {
        override fun onLoaded(productsList: Inventory.Products) {
            product = productsList.get(ProductTypes.IN_APP)

            if (!product!!.supported) {
                AlertDialog.Builder(this@BaseActivity)
                        .setTitle(R.string.warning_dialog_title)
                        .setMessage(R.string.subscription_unavailable_warning)
                        .setPositiveButton(R.string.quit, { _, _ -> finish() })
                        .setCancelable(false)
                        .show()

                Log.e(this.javaClass.toString(), "billing_not_supported")
                return
            }

            if (shouldConsume && product!!.isPurchased(Constants.IN_APP_ID)){
                shouldConsume = false
                consume(product!!.purchases[0].token)
            } else {
                inventoryLoaded(product!!.getSku(Constants.IN_APP_ID), product!!.isPurchased(Constants.IN_APP_ID))
            }
        }
    }

    private fun reloadInventory() {
        mInventory!!.load(mInventoryRequest!!, mInventoryCallback)
    }

    @Suppress("UsePropertyAccessSyntax")
    private fun purchase(inApp: String) {
        mCheckout.whenReady(object : Checkout.EmptyListener() {
            override fun onReady(requests: BillingRequests) {
                requests.purchase(ProductTypes.IN_APP, inApp, null, mCheckout.getPurchaseFlow())
            }
        })
    }

    private fun consume(product: String){
        mCheckout.whenReady(object : Checkout.EmptyListener() {
            override fun onReady(requests: BillingRequests) {
                requests.consume(product, ConsumeListener())
            }
        })
    }

    private inner class ConsumeListener : EmptyRequestListener<Any>() {
        override fun onSuccess(result: Any) {
            reloadInventory()
        }

        override fun onError(response: Int, e: java.lang.Exception) {
            super.onError(response, e)
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    fun checkPermission(permission: RequiredPermission): Boolean{
        if (ContextCompat.checkSelfPermission(this, permission.permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission.permission)) {
                PermissionDialog.show(supportFragmentManager, false, permission.code, getPermissionMessage(permission))
            } else if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(permission.permission, false)) {
                PermissionDialog.show(supportFragmentManager, true, permission.code, getPermissionMessage(permission))
            } else {
                ActivityCompat.requestPermissions(this, arrayOf(permission.permission), permission.code)
            }

            SharedPrefUtil.putBoolean(permission.permission, true)
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {

        val requiredPermission = RequiredPermission.fromInt(requestCode)

        if (requiredPermission != null && (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, requiredPermission.permission)) {
                PermissionDialog.show(supportFragmentManager, false,
                        requiredPermission.code,
                        getPermissionMessage(requiredPermission))
            }
        } else {
            permissionGranted(requiredPermission)
        }
    }

    open fun permissionGranted(permission: RequiredPermission?){}

    private fun getPermissionMessage(requiredPermission: RequiredPermission): String =
            when (requiredPermission) {
                RequiredPermission.CAMERA -> getString(R.string.permission_camera_info)
            }

}