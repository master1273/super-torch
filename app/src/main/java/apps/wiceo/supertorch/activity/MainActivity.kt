package apps.wiceo.supertorch.activity

import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat

import apps.wiceo.supertorch.R
import apps.wiceo.supertorch.application.MorseSingleton
import apps.wiceo.supertorch.application.TorchApp
import apps.wiceo.supertorch.data.MorseLetter
import apps.wiceo.supertorch.event.PlayBackEvent
import kotlinx.android.synthetic.main.activity_main.*
import apps.wiceo.supertorch.helper.MyCamera
import apps.wiceo.supertorch.event.TorchEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.alert
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.startActivity

/**
 * Created by Paul Chernenko
 * on 22.11.2017.
 */
class MainActivity: BaseActivity() {

    private var flashLightStatus = false
    private lateinit var camera: MyCamera
    private var hasFlash = false

    override fun getLayout(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hasFlash = setupViews()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        if (hasFlash)
            camera = MyCamera(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
        camera.release()
    }

    private fun setupViews(): Boolean{
        hasFlash = TorchApp.getAppContext().packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
        if (!hasFlash) {
            val alert = alert(R.string.no_flash_title, R.string.no_flash_message) {
                positiveButton(R.string.quit) { finish() }
            }
            alert.isCancelable = false
            alert.show()

            return false
        }

        torchButton.onClick{
            if (!camera.stopPlayback())
                camera.toggleFlashlight(!flashLightStatus)
        }
        sosButton.onClick{runSos()}
        codeButton.onClick {
            camera.stopPlayback()
            camera.toggleFlashlight(false)
            startActivity<MorseActivity>()
        }

        return true
    }

    private fun changeButtonDrawable(){
        torchButton.setImageResource(when (flashLightStatus){
            false -> R.drawable.light_off
            true -> R.drawable.light_on
        })
    }

    private fun runSos(){
        val letterS = MorseSingleton.getLetter('S') ?: return
        val letterO = MorseSingleton.getLetter('O') ?: return

        camera.playWord(listOf<MorseLetter>(letterS, letterO, letterS))
    }

    private fun wordPlaybackRunning(running: Boolean) = if (running) {
        sosButton.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary))
    } else {
        sosButton.setTextColor(Color.WHITE)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: PlayBackEvent) {
        wordPlaybackRunning(event.isPlaying)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: TorchEvent) {
        flashLightStatus = event.enabled
        changeButtonDrawable()
    }
}
