package apps.wiceo.supertorch.application

import android.app.Application
import android.content.Context
import apps.wiceo.supertorch.R
import apps.wiceo.supertorch.common.Constants
import com.crashlytics.android.Crashlytics
import com.google.android.gms.ads.MobileAds
import io.fabric.sdk.android.Fabric
import org.solovyev.android.checkout.Billing
import org.solovyev.android.checkout.Cache

/**
 * Created by Paul Chernenko
 * on 22.11.2017.
 */
class TorchApp: Application(){

    val mBilling = Billing(this, object : Billing.DefaultConfiguration() {
        override fun getPublicKey(): String = Constants.IN_APP_BILLING_KEY
        override fun getCache(): Cache = Billing.newCache()
    })


    init {
        instance = this
    }

    companion object {
        var instance: TorchApp? = null
        fun getAppContext() : Context = instance!!.applicationContext
    }

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        MorseSingleton.initialize(this)
        MobileAds.initialize(applicationContext, getString(R.string.ads_app_id))
    }
}