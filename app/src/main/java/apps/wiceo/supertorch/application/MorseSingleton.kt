package apps.wiceo.supertorch.application

import apps.wiceo.supertorch.data.MorseLetter
import android.content.Context
import apps.wiceo.supertorch.R
import apps.wiceo.supertorch.data.MorseCollectionJson
import com.google.gson.Gson
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringWriter


/**
 * Created by Paul Chernenko
 * on 22.11.2017.
 */
class MorseSingleton {

    companion object {

        val lettersList: MutableList<MorseLetter> = ArrayList()

        fun initialize(context: Context) {
            populateList(context)
        }

        private fun populateList(context: Context){
            val jsonString = getJsonString(context)
            val data = Gson().fromJson<MorseCollectionJson>(jsonString, MorseCollectionJson::class.java)
            if (data?.data != null) {
                data.data.mapTo(lettersList) { MorseLetter(it[1], it[2].toCharArray()) }
            }
        }

        private fun getJsonString(context: Context): String {
            val inputStream = context.resources.openRawResource(R.raw.morse)
            val writer = StringWriter()
            val buffer = CharArray(1024)
            inputStream.use { it ->
                val reader = BufferedReader(InputStreamReader(it, "UTF-8"))
                var n: Int
                n = reader.read(buffer)
                while (n != -1) {
                    writer.write(buffer, 0, n)
                    n = reader.read(buffer)
                }
            }

            return writer.toString()
        }

        fun contains(c: Char) = lettersList.any({it.letter.toLowerCase() == c.toString().toLowerCase()})

        fun getLetter(letter: Char): MorseLetter? =
                lettersList.firstOrNull { letter.toString().toUpperCase() == it.letter.toUpperCase() }
    }
}