package apps.wiceo.supertorch.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDialogFragment
import apps.wiceo.supertorch.R
import apps.wiceo.supertorch.common.RequiredPermission
import apps.wiceo.supertorch.util.DialogUtil

/**
 * Created by Paul Chernenko
 * on 22.11.2017.
 */
class PermissionDialog: AppCompatDialogFragment() {

    companion object {
        val EXTRA_FROM_SETTINGS = "fromSettings"
        val EXTRA_PERMISSION_CODE = "permissionCode"
        val EXTRA_PERMISSION_MESSAGE = "permissionMessage"


        private val TAG = PermissionDialog::class.java.simpleName

        fun show(fm: FragmentManager, fromSettings: Boolean, requiredPermissionCode: Int, message: String) {
            var dialog: PermissionDialog? = fm.findFragmentByTag(TAG) as PermissionDialog
            if (dialog == null) {
                val args = Bundle()
                args.putBoolean(EXTRA_FROM_SETTINGS, fromSettings)
                args.putInt(EXTRA_PERMISSION_CODE, requiredPermissionCode)
                args.putString(EXTRA_PERMISSION_MESSAGE, message)

                dialog = PermissionDialog()
                dialog.isCancelable = false
                dialog.arguments = args
                fm.beginTransaction().add(dialog, TAG).commitAllowingStateLoss()
            }
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val fromSettings = arguments.getBoolean(EXTRA_FROM_SETTINGS, false)
        val message = arguments.getString(EXTRA_PERMISSION_MESSAGE)
        val permissionCode = arguments.getInt(EXTRA_PERMISSION_CODE, -1)
        val requiredPermission = RequiredPermission.fromInt(permissionCode)


        val builder = AlertDialog.Builder(activity)
        builder.setMessage(message)
                .setTitle(R.string.permission_title)
        builder.setCancelable(false)
        builder.setPositiveButton(R.string.grant) { _, _ ->
            if (requiredPermission != null)
                if (fromSettings)
                    DialogUtil.showApplicationSettings(activity, requiredPermission.code)
                else
                    ActivityCompat.requestPermissions(activity, arrayOf(requiredPermission.permission), requiredPermission.code)
        }
        builder.setNegativeButton(R.string.cancel) { dialog, _ -> dialog.dismiss() }
        return builder.create()
    }

}