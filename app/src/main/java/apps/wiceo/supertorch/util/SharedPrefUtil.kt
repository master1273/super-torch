package apps.wiceo.supertorch.util

import android.content.SharedPreferences
import android.preference.PreferenceManager
import apps.wiceo.supertorch.application.TorchApp

/**
 * Created by Paul Chernenko
 * on 17.11.2017.
 */
class SharedPrefUtil{
    companion object {

        fun getSharedPreferences(): SharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(TorchApp.getAppContext())

        /***********************************************/
        fun putLong(key: String, value: Long) {
            val editor = getSharedPreferences().edit()
            editor.putLong(key, value)
            editor.apply()
        }

        fun putInt(key: String, value: Int) {
            val editor = getSharedPreferences().edit()
            editor.putInt(key, value)
            editor.apply()
        }

        fun putBoolean(key: String, value: Boolean){
            val editor = getSharedPreferences().edit()
            editor.putBoolean(key, value)
            editor.apply()
        }

        /***********************************************/
        fun getLong(key: String): Long = getSharedPreferences().getLong(key, 0)
        fun getInt(key: String): Int = getSharedPreferences().getInt(key, 0)
        fun getBoolean(key: String): Boolean = getSharedPreferences().getBoolean(key, false)
    }
}