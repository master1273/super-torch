package apps.wiceo.supertorch.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings

/**
 * Created by Paul Chernenko
 * on 22.11.2017.
 */
class DialogUtil{
    companion object {
        fun showApplicationSettings(context: Context, code: Int) {
            val uri = Uri.fromParts("package", context.packageName, null)
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            intent.data = uri
            if (intent.resolveActivity(context.packageManager) != null) {
                (context as Activity).startActivityForResult(intent, code)
            }
        }
    }
}