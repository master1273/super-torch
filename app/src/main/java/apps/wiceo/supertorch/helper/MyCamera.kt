package apps.wiceo.supertorch.helper

import android.content.Context
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraCharacteristics.FLASH_INFO_AVAILABLE
import android.hardware.camera2.CameraManager
import apps.wiceo.supertorch.common.Constants
import apps.wiceo.supertorch.data.MorseLetter
import apps.wiceo.supertorch.event.PlayBackEvent
import apps.wiceo.supertorch.event.TorchEvent
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.doAsync
import java.util.concurrent.Future

/**
 * Created by Paul Chernenko
 * on 23.11.2017.
 */
class MyCamera constructor(context: Context): CameraManager.TorchCallback() {

    private val manager = context.getSystemService(Context.CAMERA_SERVICE) as CameraManager
    private var cameraId: String? = null
    private var playback: Future<Unit>? = null

    init {
        for (item in manager.cameraIdList){
            if (manager.getCameraCharacteristics(item)[FLASH_INFO_AVAILABLE]) {
                cameraId = item
                manager.registerTorchCallback(this, null)
                break
            }
        }
    }

    fun toggleFlashlight(enable: Boolean): Boolean {
        try {
            manager.setTorchMode(cameraId!!, enable)
        } catch (e: CameraAccessException) {
            return !enable
        }
        return enable

    }

    override fun onTorchModeChanged(cameraId: String?, enabled: Boolean) {
        super.onTorchModeChanged(cameraId, enabled)

        if (cameraId != null && this.cameraId != null && this.cameraId == cameraId){
            EventBus.getDefault().post(TorchEvent(enabled))
        }
    }

    fun release(){
        manager.unregisterTorchCallback(this)
    }

    fun playWord(morseLetters: List<MorseLetter>){
        if (!stopPlayback()){
            toggleFlashlight(false)
            EventBus.getDefault().post(PlayBackEvent(true))
            playback = doAsync {
                playWordOnBackgroundThread(morseLetters)
                EventBus.getDefault().post(PlayBackEvent(false))
            }
        }
    }

    fun stopPlayback(): Boolean {
        if (playbackRunning()){
            playback!!.cancel(true)
            playback = null
            toggleFlashlight(false)
            EventBus.getDefault().post(PlayBackEvent(false))
            return true
        }
        return false
    }

    fun playbackRunning() = playback != null && !playback!!.isDone

    @Throws(InterruptedException::class)
    private fun playLetterOnBackgroundThread(morseLetter: MorseLetter){
        var shouldSleepFirst = false
        for (c in morseLetter.sequence){
            if (shouldSleepFirst){
                Thread.sleep(Constants.DOT_INTERVAL)
            } else {
                shouldSleepFirst = true
            }
            if (c == '.'){
                toggleFlashlight(true)
                Thread.sleep(Constants.DOT_INTERVAL)
                toggleFlashlight(false)
            } else if (c == '-') {
                toggleFlashlight(true)
                Thread.sleep(Constants.PERIOD_INTERVAL)
                toggleFlashlight(false)
            }
        }
    }

    @Throws(InterruptedException::class)
    private fun playWordOnBackgroundThread(morseLetters: List<MorseLetter>){
        var shouldSleepFirst = false
        for (morseLetter in morseLetters){
            if (shouldSleepFirst) {
                Thread.sleep(Constants.PERIOD_INTERVAL)
            } else {
                shouldSleepFirst = true
            }
            playLetterOnBackgroundThread(morseLetter)
        }
    }
}