package apps.wiceo.supertorch.common

import android.Manifest

/**
 * Created by Paul Chernenko
 * on 22.11.2017.
 */
enum class RequiredPermission(val code: Int, val permission: String) {
    CAMERA(0, Manifest.permission.CAMERA);

    companion object {
        fun fromInt(value: Int): RequiredPermission? {
            return when (value) {
                5 -> CAMERA
                else -> null
            }
        }
    }
}