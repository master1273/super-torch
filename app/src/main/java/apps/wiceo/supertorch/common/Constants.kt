package apps.wiceo.supertorch.common

/**
 * Created by Paul Chernenko
 * on 24.11.2017.
 */
class Constants {
    companion object {
        const val DOT_INTERVAL = 250L
        const val PERIOD_INTERVAL = DOT_INTERVAL * 3
        const val IN_APP_BILLING_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgjVwGylmLzTk1T4fSeMRjMW9MLRFmOIhfzZxoVbOdXmYKRvlYHYAL1lZwwuaZB+ARZ3UNR4BEX2bWdue87z1nxJtxuQqa3rJd0vuQcqB5MjV0y9HcvpVOV5Toki6kVAZSQoURBszUxMmbjybtvB0T47YDL2L1FPrava+24qCD64PKd/agzhdYaXcyeNbzogS22IPQzVCsMYoCfLv+RVCqDRPxV1sI4lt42D2gjBII9jELeBvXzONDn1FPqWJd5zQeScjzhi5vPSDKgni+Wah/XBcOE+J5EbjAeQvCxPR+2pkns2npIecE4n8ScNmMUOydtECDO7cxFzMHwSSGRTVZwIDAQAB"
        const val IN_APP_ID = "apps.wiceo.supertorch.ads_free"
        //"android.test.purchased" - id for testing

        const val ADD_DISPLAY_PERIOD = 3
        const val USE_TEST_ADS = false
    }
}