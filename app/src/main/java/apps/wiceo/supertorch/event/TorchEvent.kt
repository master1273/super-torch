package apps.wiceo.supertorch.event

/**
 * Created by Paul Chernenko
 * on 23.11.2017.
 */
class TorchEvent(val enabled: Boolean)