package apps.wiceo.supertorch.event

/**
 * Created by Paul Chernenko
 * on 24.11.2017.
 */
class PlayBackEvent(val isPlaying: Boolean)