package apps.wiceo.supertorch.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import apps.wiceo.supertorch.R
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.text.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import apps.wiceo.supertorch.activity.MorseActivity
import kotlinx.android.synthetic.main.fragment_word.*
import apps.wiceo.supertorch.application.MorseSingleton
import apps.wiceo.supertorch.data.MorseLetter
import apps.wiceo.supertorch.event.PlayBackEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.toast


/**
 * Created by Paul Chernenko
 * on 29.11.2017.
 */
class WordFragment: Fragment() {

    companion object {
        fun newInstance() = WordFragment()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.fragment_word, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        input.filters = arrayOf(InputFilter { source, start, end, dest, dstart, dend ->
            (start until end)
                    .filterNot { MorseSingleton.contains(source[it]) }
                    .forEach { return@InputFilter "" }
            null
        })

        input.afterTextChanged { playButton.visibility = if (input.text.toString().isEmpty()) GONE else VISIBLE }

        playButton.onClick { playWord(input.text.toString()) }
    }

    private fun playWord(text: String){
        val lettersList = ArrayList<MorseLetter>()
        for (c in text) {
            val letter: MorseLetter? = MorseSingleton.getLetter(c)
            if (letter != null){
                lettersList.add(letter)
            } else {
                toast("Text contains illegal letters")
                return
            }
        }

        (activity as MorseActivity).playWord(lettersList)

    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (activity != null)
            if (isVisibleToUser){
                input.requestFocus()
                showKeyboard(activity)
            } else {
                hideKeyboard(activity)
            }
    }

    private fun hideKeyboard(context: Context) {
        try {
            (context as Activity).window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            if (context.currentFocus != null && context.currentFocus!!.windowToken != null) {
                (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(context.currentFocus!!.windowToken, 0)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showKeyboard(context: Context) {
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

    fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
        this.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(editable: Editable?) {
                afterTextChanged.invoke(editable.toString())
            }
        })
    }

    private fun wordPlaybackRunning(running: Boolean) = if (running) {
        playButton.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary))
        playButton.setText(getString(R.string.stop))
    } else {
        playButton.setTextColor(Color.WHITE)
        playButton.setText(getString(R.string.play))
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        hideKeyboard(activity)
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: PlayBackEvent) {
        wordPlaybackRunning(event.isPlaying)
    }

}