package apps.wiceo.supertorch.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import apps.wiceo.supertorch.R
import apps.wiceo.supertorch.adapter.MorseGridAdapter
import apps.wiceo.supertorch.event.PlayBackEvent
import kotlinx.android.synthetic.main.fragment_alphabet.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * Created by Paul Chernenko
 * on 29.11.2017.
 */
class AlphabetFragment: Fragment() {

    companion object {
        fun newInstance() = AlphabetFragment()
    }

    var adapter: MorseGridAdapter? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.fragment_alphabet, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val gridLayoutManager = GridLayoutManager(activity, 4)
        gridLayoutManager.orientation = GridLayoutManager.VERTICAL
        lettersGrid.layoutManager = gridLayoutManager
        lettersGrid.setHasFixedSize(true)

        adapter = MorseGridAdapter(activity)
        lettersGrid.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    private fun wordPlaybackRunning(isPlaying: Boolean){
        if (adapter != null)
            adapter!!.playbackStatus(isPlaying)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: PlayBackEvent) {
        wordPlaybackRunning(event.isPlaying)
    }

}