package apps.wiceo.supertorch.adapter

import android.content.Context
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import apps.wiceo.supertorch.R
import apps.wiceo.supertorch.activity.MorseActivity
import apps.wiceo.supertorch.application.MorseSingleton.Companion.lettersList
import apps.wiceo.supertorch.data.MorseLetter
import kotlinx.android.synthetic.main.item_morse_letter.view.*
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * Created by Paul Chernenko
 * on 29.11.2017.
 */
class MorseGridAdapter(private val context: Context): RecyclerView.Adapter<MorseGridAdapter.ViewHolder>() {

    private val dataList = ArrayList<LetterItem>()

    init {
        lettersList.mapTo(dataList) { LetterItem(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MorseGridAdapter.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_morse_letter, parent, false), context)


    override fun onBindViewHolder(holder: MorseGridAdapter.ViewHolder, position: Int) {
        holder.bindItems(dataList[position])
    }

    override fun getItemCount(): Int = lettersList.size

    class ViewHolder(itemView: View, private val context: Context) : RecyclerView.ViewHolder(itemView) {

        private var letterItem: LetterItem? = null

        fun bindItems(letterItem: LetterItem) {
            this.letterItem = letterItem
            itemView.letter.text = letterItem.letter.letter
            itemView.letter.onClick { onLetterClick(it as Button) }
            itemView.letter.setTextColor(getTextColor())
        }

        private fun onLetterClick(button: Button){
            if ((context as MorseActivity).playLetter(letterItem!!.letter)) {
                letterItem!!.isPressed = true
                button.setTextColor(getTextColor())
            }
        }

        private fun getTextColor() = when(letterItem!!.isPressed){
            true -> ContextCompat.getColor(context, R.color.colorPrimary)
            false -> Color.WHITE
        }
    }

    fun playbackStatus(isPlaying: Boolean){
        if (!isPlaying) {
            dataList.forEach({ it.isPressed = false })
            notifyDataSetChanged()
        }
    }


    data class LetterItem(val letter: MorseLetter, var isPressed: Boolean = false)
}