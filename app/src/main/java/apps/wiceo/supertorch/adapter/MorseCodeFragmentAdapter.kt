package apps.wiceo.supertorch.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import apps.wiceo.supertorch.R
import apps.wiceo.supertorch.application.TorchApp
import apps.wiceo.supertorch.fragment.AlphabetFragment
import apps.wiceo.supertorch.fragment.WordFragment

/**
 * Created by Paul Chernenko
 * on 29.11.2017.
 */
class MorseCodeFragmentAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    override fun getCount(): Int = 2

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> AlphabetFragment.newInstance()
        1 -> WordFragment.newInstance()
        else -> throw IllegalArgumentException("Unknown page position: " + position)
    }

    override fun getPageTitle(position: Int): CharSequence = when (position) {
        0 -> TorchApp.getAppContext().resources.getString(R.string.by_letter)
        1 -> TorchApp.getAppContext().resources.getString(R.string.by_word)
        else -> throw IllegalArgumentException("Unknown page position: " + position)
    }
}