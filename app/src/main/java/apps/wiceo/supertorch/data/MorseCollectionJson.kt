package apps.wiceo.supertorch.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Paul Chernenko
 * on 22.11.2017.
 */
class MorseCollectionJson{
    @SerializedName("data")
    @Expose
    val data: List<Array<String>>? = null
}