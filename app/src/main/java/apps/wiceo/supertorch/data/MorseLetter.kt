package apps.wiceo.supertorch.data

import java.util.*

/**
 * Created by Paul Chernenko
 * on 22.11.2017.
 */
class MorseLetter(val letter: String, val sequence: CharArray)